# SnapBee

Detection of blood vessel segmentation for bee's wings

python3 run.py -i inputDIR (-o outputDIR)
python3 run.py --image inputIMAGE (-o outputDIR) (for single image)

the csv file generated format for each point: height coordinate (y) , width coordinate (x)