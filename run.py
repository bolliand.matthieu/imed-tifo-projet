import argparse
import numpy as np
import skimage as sk
from skimage import morphology as skm
import cv2
import os
import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter

def differenceOfGaussian(img, k1, k2):
    
    g1 = cv2.blur(img, (k1, k1))
    g2 = cv2.blur(img, (k2, k2))
    
    return g1 - g2

def my_subtract(a,b):
    
    res = np.copy(a)
    if a.shape[0] % 2 == 0 or a.shape[1] != a.shape[0] or b.shape[0] % 2 == 0 or b.shape[1] != b.shape[0]:
        return
    dif = abs(a.shape[0] // 2 - b.shape[0] // 2)
    for i in range(b.shape[0]):
        for j in range(b.shape[0]):
            res[dif+i,dif+j] -= b[i,j]
    return res
    
def get_centroids(img):
    
    im = cv2.dilate(img, skm.disk(5) ,iterations = 2)
    _,_,_,centroids = cv2.connectedComponentsWithStats(im, connectivity=8)
    return centroids

def detection(path):
    
    im = cv2.cvtColor(cv2.imread(path), cv2.COLOR_BGR2LAB)
    im_l,_,_ = cv2.split(im)
    im_gau = cv2.GaussianBlur(im_l, (5,5), 3.0)
    im_diff = differenceOfGaussian(im_gau, 25, 50)
    im_treshold = np.uint8(im_diff > 60)
    im_top = cv2.morphologyEx(im_treshold, cv2.MORPH_TOPHAT, skm.disk(10))
    im_top = im_treshold - im_treshold * im_top
    im_morph = cv2.dilate(im_top, skm.disk(2), iterations=4)
    im_morph = cv2.erode(im_morph, skm.disk(2), iterations=8)
    im_morph = cv2.dilate(im_morph, skm.disk(2), iterations=2)
    im_label = sk.measure.label(im_morph)
    tmp = np.where(np.bincount(im_label.flatten()) > 25000)[0][1:]
    im_filter = np.zeros_like(im_label).astype(bool)
    for value in tmp:
        im_filter = im_filter | (im_label == value)
    im_filter_morph = cv2.dilate(np.uint8(im_filter), skm.disk(1) ,iterations = 10)
    im_filter_morph = cv2.erode(np.uint8(im_filter_morph), skm.disk(1) ,iterations = 7)
    im_filter_morph = cv2.dilate(np.uint8(im_filter_morph), skm.disk(1) ,iterations = 5)
    im_filter_morph = cv2.erode(np.uint8(im_filter_morph), skm.disk(1) ,iterations = 3)
    skeleton = skm.skeletonize(im_filter_morph)
    ring = my_subtract(skm.square(23), skm.square(21))
    pading = len(ring)
    points = np.zeros_like(im_l)
    for i in range(0, skeleton.shape[1] - pading, int(pading / 1.5)):
        for j in range(0, skeleton.shape[0] - pading, int(pading / 1.5)):
            window = skeleton[j:j + pading, i:i + pading] * ring
            if window.sum() < 3:
                continue
            labels = len(np.bincount(sk.measure.label(window).flatten()))
            if labels > 3 :
                points[j + pading // 2, i + pading // 2] = 1
    return get_centroids(points)


def main(args):
    
    save_dir = "RESULTS" if args.output is None  else args.output
    
    if not os.path.exists(save_dir):
        os.mkdir(save_dir)
    
    if args.input is None:
        if args.image[-4:] == ".jpg" or args.image[-4:] == ".png":
            points = detection(args.image)[...,[1,0]]
            start = args.image.rfind('/') + 1
            np.savetxt(save_dir + "/" + args.image[start:-4] + ".csv",points, delimiter=",", fmt='%.3e')
            print(path, "DONE")
        elif args.image[-5:] == ".jpeg":
            print(args.input+ "/" +args.image)
            points = detection(args.input+ "/" +args.image)[...,[1,0]]
            start = args.image.rfind('/') + 1
            np.savetxt(save_dir + "/" + args.image[start:-5] + ".csv",points, delimiter=",", fmt='%.3e')
            print(path, "DONE")
    else :
        for path in os.listdir(args.input):
            if path[-4:] == ".jpg" or path[-4:] == ".png":
                points = detection(args.input+ "/" +path)[...,[1,0]]
                np.savetxt(save_dir + "/" + path[:-4] + ".csv",points, delimiter=",", fmt='%.3e')
                print(path, "DONE")
            elif path[-5:] == ".jpeg":
                points = detection(args.input+ "/" +path)[...,[1,0]]
                np.savetxt(save_dir + "/" + path[:-5] + ".csv",points, delimiter=",", fmt='%.3e')
                print(path, "DONE")
    

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser()
    parser.add_argument("--input","-i", type=str, help='directory of jpg images')
    parser.add_argument("--image", type=str, help='single jpg image path')
    parser.add_argument('--output', '-o', type=str, help='Output directory (optional, RESULTS by default)')
    args = parser.parse_args()
    
    if args.input is None and args.image is None:
        print("No input")
    else:
        main(args)